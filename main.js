document.getElementById('waitingListForm').addEventListener('submit', function(event) {
  event.preventDefault();
  var email = document.getElementById('email').value;
  document.getElementById('message').textContent = 'Thank you for joining the waiting list, ' + email;
});
